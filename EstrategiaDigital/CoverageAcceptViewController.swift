//
//  CoverageAcceptViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 28/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class CoverageAcceptViewController: UIViewController {
    
    @IBOutlet weak var wifiImageView: UIImageView!
   
    @IBOutlet weak var phoneImageView: UIImageView!
    
    @IBOutlet weak var plusImageView: UIImageView!
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        wifiImageView.image = wifiImageView.image!.withRenderingMode(.alwaysTemplate)
        wifiImageView.tintColor = UIColor(netHex: Colors.color_grey)
        
        phoneImageView.image = phoneImageView.image!.withRenderingMode(.alwaysTemplate)
        phoneImageView.tintColor =  UIColor(netHex: Colors.color_grey)
        
        
        plusImageView.image = plusImageView.image!.withRenderingMode(.alwaysTemplate)
        plusImageView.tintColor =  UIColor(netHex: Colors.color_grey)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func nextButton(_ sender: Any) {
       
    }
    
}
