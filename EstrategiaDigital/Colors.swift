//
//  Colors.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/21/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit

//REGEX
// FIND
//<color name="(\w+)">#(\w+)</color>
// REPLACE
//static let \1 : Int = 0x\2

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

class Colors {
    static let color_grey : Int = 0x575B68
    static let color_white_light : Int = 0xCFD2E4
}
