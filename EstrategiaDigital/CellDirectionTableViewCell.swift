//
//  CellDirectionTableViewCell.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 27/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class CellDirectionTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLocationLabel: UILabel!

    @IBOutlet weak var iconAccepImageView: UIImageView!
    @IBOutlet weak var directionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
