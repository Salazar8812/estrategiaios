//
//  AdditionalServiceViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 18/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class AdditionalServiceViewController: UIViewController ,iCarouselDataSource, iCarouselDelegate{

    @IBOutlet weak var pagerAdditionalServices: iCarousel!
    
    var selectedIndex : Int!
    var indice : Int!

    var additionalServiceArray = [AdditionalServices]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateAdditionalServices()
        
        pagerAdditionalServices.type = iCarouselType.linear
        pagerAdditionalServices.delegate = self
        pagerAdditionalServices.dataSource = self
        pagerAdditionalServices .reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func populateAdditionalServices(){
        let c : AdditionalServices = AdditionalServices()
        c.mainImageAdditional = "ic_gameOfThrones"
        c.titleImageAdditional = "ic_title_hbo"
        c.legendAdditional = "Canal con estrenos de películas ganadoras en los mejores festivales de cine, documentale, programación alternativa y más por sólo $159 al mes."
        additionalServiceArray.append(c)
        
        let c2 : AdditionalServices = AdditionalServices()
        c2.mainImageAdditional = "ic_theWalkingDead"
        c2.titleImageAdditional = "ic_title_hbo"
        c2.legendAdditional = "Canales con las mejores películas independientes, series de alto impacto, producciones originales, documentales y más por sólo $159 mensuales."
        additionalServiceArray.append(c2)
    }
    
    

    /*-------------------------- Default Generate Methods iCarousel ------------------------*/
    
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return additionalServiceArray.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let view = Bundle.main.loadNibNamed("AditionalServiceCollectionReusableView", owner: self, options: nil)![0] as! AditionalServiceCollectionReusableView
        /*view.frame.size = CGSize(width: self.view.frame.size.width/2, height: 83.0)
         view.backgroundColor = UIColor.lightGray*/
        
        view.imageAdditionalImageView.image = UIImage(named: additionalServiceArray[index].mainImageAdditional!)
        view.logoAdditionalImageView.image = UIImage(named: additionalServiceArray[index].titleImageAdditional!)
        view.legendLabel.text = additionalServiceArray[index].legendAdditional!
        
        return view
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        selectedIndex = index
        //self .performSegue(withIdentifier: "imageDisplaySegue", sender: nil)
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        indice = carousel.currentItemIndex
        
        do {
            if(carousel.currentItemIndex == 0){
                /*numbCard = cardsArray[0].cardNumber
                 print("Numero de Tarjeta", numbCard!)*/
            }else{
                /*numbCard = cardsArray[carousel.currentItemIndex].cardNumber
                 print("Numero de Tarjeta", numbCard!)*/
            }
            
        } catch let error as String{
            print(error)
        }
    }
    
    /*-------------------------- End Default Generate Methods iCarousel ------------------------*/
    
}
