//
//  PaymentMethodViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 21/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class PaymentMethodViewController: UIViewController {

    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var typeMethodSegment: UISegmentedControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
