//
//  PackagesViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 05/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class PackagesViewController: UIViewController,iCarouselDataSource, iCarouselDelegate {

    @IBOutlet weak var listPackageCarousel: iCarousel!
    @IBOutlet weak var listDescriptionTableView: UITableView!
    
    var packageArray = [Package]()
    var descriptionArray = [DescriptionPackage]()
    var selectedIndex : Int!
    var indice : Int!
    var descriptionPDataSoure : DescriptionPackageDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populate_Description()
        populate_Package()
        
        listPackageCarousel.type = iCarouselType.linear
        listPackageCarousel.delegate = self
        listPackageCarousel.dataSource = self
        listPackageCarousel .reloadData()
        
        
        let nib = UINib(nibName: "DescriptionPackageTableViewCell", bundle: nil)
        listDescriptionTableView.register(nib, forCellReuseIdentifier: "DescriptionPackageTableViewCell")
        descriptionPDataSoure = DescriptionPackageDataSource(tableView: self.listDescriptionTableView)
        descriptionPDataSoure?.update(coleccionInfo: descriptionArray)
        self.listDescriptionTableView.reloadData()
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
    }

    func populate_Package(){
        let c : Package = Package()
        c.id = "4152"
        c.descriptionPackage = "<p style=\"font-family:verdana;color:white;\">+ 250 canales, 95 en HD $1,079 x 6 meses</p>"
        c.typePackage = "ic_initial_package"
        c.megasPackage = "DOBLE DE MEGAS "
        c.listDescription = descriptionArray
        packageArray.append(c)
        
        
        let c1 : Package = Package()
        c1.id = "4152"
        c1.descriptionPackage = "<p style=\"font-family:verdana;color:white;\">+ 250 canales, 95 en HD $1,079 x 6 meses</p>"
        c1.typePackage = "ic_esential_package"
        c1.megasPackage = "DOBLE DE MEGAS "
        c1.listDescription = descriptionArray
        packageArray.append(c1)
        
        
        let c2 : Package = Package()
        c2.id = "4152"
        c2.descriptionPackage = "<p style=\"font-family:verdana;color:white;\">+ 250 canales, 95 en HD $1,079 x 6 meses</p>"
        c2.typePackage = "ic_basic_package"
        c2.megasPackage = "DOBLE DE MEGAS "
        c2.listDescription = descriptionArray
        packageArray.append(c2)
        
        
        let c3 : Package = Package()
        c3.id = "4152"
        c3.descriptionPackage = "<p style=\"font-family:verdana;color:white;\">+ 250 canales, 95 en HD $1,079 x 6 meses</p>"
        c3.typePackage = "ic_practice_package"
        c3.megasPackage = "DOBLE DE MEGAS "
        c3.listDescription = descriptionArray
        packageArray.append(c3)
        
        
        let c4 : Package = Package()
        c4.id = "4152"
        c4.descriptionPackage = "<p style=\"font-family:verdana;color:white;\">+ 250 canales, 95 en HD $1,079 x 6 meses</p>"
        c4.typePackage = "ic_package_family"
        c4.megasPackage = "DOBLE DE MEGAS "
        c4.listDescription = descriptionArray
        packageArray.append(c4)
        
        
        let c5 : Package = Package()
        c5.id = "4152"
        c5.descriptionPackage = "<p style=\"font-family:verdana;color:white;\">+ 250 canales, 95 en HD $1,079 x 6 meses</p>"
        c5.typePackage = "ic_total_package"
        c5.megasPackage = "DOBLE DE MEGAS "
        c5.listDescription = descriptionArray
        packageArray.append(c5)
        
        let c6 : Package = Package()
        c6.id = "4152"
        c6.descriptionPackage = "<p style=\"font-family:verdana;color:white;\">+ 250 canales, 95 en HD $1,079 x 6 meses</p>"
        c6.typePackage = "ic_premium_package"
        c6.megasPackage = "DOBLE DE MEGAS "
        c6.listDescription = descriptionArray
        packageArray.append(c6)
        
        
        let c7 : Package = Package()
        c7.id = "4152"
        c7.descriptionPackage = "<p style=\"font-family:verdana;color:white;\">+ 250 canales, 95 en HD $1,079 x 6 meses</p>"
        c7.typePackage = "ic_without_limits_package"
        c7.megasPackage = "DOBLE DE MEGAS "
        c7.listDescription = descriptionArray
        packageArray.append(c7)
    }
    
    func populate_Description(){
        let c : DescriptionPackage = DescriptionPackage()
        c.descriptionP = "<p style=\"font-family:verdana;color:white;\">Servicio para 1 TV, Donde Sea y Cuando Sea (regresa tu programación sin costo extra), Televisón interactiva.</p>"
        c.id = "12"
        c.iconDescription = "ic_tv_package"
        descriptionArray.append(c)
        
        let c2 : DescriptionPackage = DescriptionPackage()
        c2.descriptionP = "<p style=\"font-family:verdana;color:white;\">Elige entre uno de estos dos: FOX Premium o HBO MAX</p>"
        c2.id = "12"
        c2.iconDescription = "ic_channels_package"
        descriptionArray.append(c2)
        
        
        let c3 : DescriptionPackage = DescriptionPackage()
        c3.descriptionP = "<p style=\"font-family:verdana;color:white;\">On Demand Renta los estrenos más destacados desde $45 pesos</p>"
        c3.id = "12"
        c3.iconDescription = "ic_start_package"
        descriptionArray.append(c3)
        
        let c4 : DescriptionPackage = DescriptionPackage()
        c4.descriptionP = "<p style=\"font-family:verdana;color:white;\">Telefonía a números fijos y celulares de México, Estados Unidos y Canadá incluida. Incluye Softphone.</p>"
        c4.id = "12"
        c4.iconDescription = "ic_phone_package"
        descriptionArray.append(c4)
        
        let c5 : DescriptionPackage = DescriptionPackage()
        c5.descriptionP = "<p style=\"font-family:verdana; color:white;\">$1,159 Pronto Pago, $1,359 Precio de lista * Consulta términos y condiciones <strong>Promoción $1,079*</strong></p>"
        c5.id = "12"
        c5.iconDescription = "ic_price_package"
        descriptionArray.append(c5)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func nextButton(_ sender: Any) {
    
    }
    
    
    /*-------------------------- Default Generate Methods iCarousel ------------------------*/
    
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return packageArray.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let view = Bundle.main.loadNibNamed("ItemPackageCollectionReusableView", owner: self, options: nil)![0] as! ItemPackageCollectionReusableView
        /*view.frame.size = CGSize(width: self.view.frame.size.width/2, height: 83.0)
         view.backgroundColor = UIColor.lightGray*/
        
        view.typePackageImageView.image = UIImage(named: packageArray[index].typePackage!)
        return view
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        selectedIndex = index
        //self .performSegue(withIdentifier: "imageDisplaySegue", sender: nil)
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        indice = carousel.currentItemIndex
        
        do {
            if(carousel.currentItemIndex == 0){
                /*numbCard = cardsArray[0].cardNumber
                 print("Numero de Tarjeta", numbCard!)*/
            }else{
                /*numbCard = cardsArray[carousel.currentItemIndex].cardNumber
                 print("Numero de Tarjeta", numbCard!)*/
            }
            
        } catch let error as String{
            print(error)
        }
    }
    
    /*-------------------------- End Default Generate Methods iCarousel ------------------------*/
    
}
