//
//  AdditionalServices.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 18/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import Foundation
import ObjectMapper

class AdditionalServices: NSObject, Mappable{
    
    var mainImageAdditional: String?
    var titleImageAdditional: String?
    var legendAdditional: String?
    
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        mainImageAdditional		<- map["description"]
        titleImageAdditional		<- map["id"]
        legendAdditional		<- map["place_id"]
    }
 
}
