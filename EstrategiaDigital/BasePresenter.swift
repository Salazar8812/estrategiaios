//
//  BasePresenter.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 27/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class BasePresenter: NSObject {
    
    override init(){
        super.init()
    }
    
    func loadPresenter(){
        
    }
    
    func unloadPresenter(){
        
    }
    
}

