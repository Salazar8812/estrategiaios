//
//  ConfirmLocationViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 26/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class ConfirmLocationViewController: UIViewController {
   
    
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var hoodTextField: UITextField!
    @IBOutlet weak var localTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var numberIntTextField: UITextField!
    @IBOutlet weak var numberExtTextField: UITextField!
    
    var dataAddressComplete : [AddressComponent] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch dataAddressComplete.count {
            
        case 5:
            zipCodeTextField.text = dataAddressComplete[5].long_name
            streetTextField.text = dataAddressComplete[0].long_name
            hoodTextField.text = dataAddressComplete[1].long_name
            localTextField.text = dataAddressComplete[3].long_name
            stateTextField.text = dataAddressComplete[2].long_name
            cityTextField.text = dataAddressComplete[4].long_name
            
            break
            
        case 4:
            zipCodeTextField.text = dataAddressComplete[3].long_name
            streetTextField.text = dataAddressComplete[0].long_name
            hoodTextField.text = dataAddressComplete[1].long_name
            localTextField.text = dataAddressComplete[1].long_name
            stateTextField.text = dataAddressComplete[2].long_name
            cityTextField.text = dataAddressComplete[2].long_name
            break
        
        case 3:
            hoodTextField.text = dataAddressComplete[0].long_name
            localTextField.text = dataAddressComplete[0].long_name
            stateTextField.text = dataAddressComplete[1].long_name
            cityTextField.text = dataAddressComplete[2].long_name
            break
            
        default:
            break
        }
    }
    
    @IBAction func nextButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller: CoverageAcceptViewController? = storyboard.instantiateViewController(withIdentifier: "CoverageAcceptViewController") as? CoverageAcceptViewController
        navigationController?.pushViewController(controller!, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
       self.navigationController?.isNavigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
