//
//  MapLocationSearchViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 26/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import GoogleMaps

class MapLocationSearchViewController: UIViewController,UITextFieldDelegate,PredictionAddressDelegate,DirectionTableViewDelegate, GeometryDelegate {

    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var googleContainerMapView: UIView!
    @IBOutlet weak var serchTextField: UITextField!
    @IBOutlet weak var resulTableView: UITableView!
    @IBOutlet weak var nextContinueButton: UIButton!
    
    var googleMapView : GMSMapView!
    var autoCompleteDirection: [String] = []
    var autoComplete: [String] = []
    var addrressComplete : [AddressComponent] = []
    
    var predictionPresenter : PredictionAddressPresenter?
    var geometryPresenter : GeometryPresenter?
    
    var directionDataSource : DirectionAutocompleteDataSource?
    
    @IBOutlet weak var dinamicallyNSConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        serchTextField.delegate = self
        ViewUtils.setVisibility(view: self.resulTableView, visibility: .INVISIBLE)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        predictionPresenter = PredictionAddressPresenter(delegate: self)
        geometryPresenter = GeometryPresenter(delegate: self)
        
        let nib = UINib(nibName: "CellDirectionTableViewCell", bundle: nil)
        resulTableView.register(nib, forCellReuseIdentifier: "CellDirectionTableViewCell")
        
        directionDataSource = DirectionAutocompleteDataSource(tableView: self.resulTableView,tableViewDelegate: self)
        
        self.googleMapView = GMSMapView(frame: self.googleContainerMapView.frame)
        self.view.addSubview(self.googleMapView)
        self.view.addSubview(self.searchContainerView)
        self.view.addSubview(self.resulTableView)
        self.view.addSubview(self.nextContinueButton)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let substring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        

        if(substring.characters.count < 1){
            ViewUtils.setVisibility(view: self.resulTableView, visibility: .INVISIBLE)
        }
        
        if(substring.characters.count > 4 ){
            ViewUtils.setVisibility(view: self.resulTableView, visibility: .VISIBLE)
            searchAutocompleteDirection(substring)
        }
        
        return true
    }
    
    
    func searchAutocompleteDirection(_ substring: String) {
        autoComplete.removeAll(keepingCapacity: false)
        print(substring)
        predictionPresenter?.getPredictionAddress(googleMapsKey: "AIzaSyA96e9yso7kBeWRujnxmOClOrKcatknH90", keyword: substring)
    }
    
    func onSuccessPrediction(collectionAddress: [PredictionAddress]) {
        directionDataSource?.update(coleccionInfo: collectionAddress)
        self.resulTableView.reloadData()
        
        resulTableView.layoutIfNeeded()
        dinamicallyNSConstraint.constant = resulTableView.contentSize.height
        resulTableView.layoutIfNeeded()
    }
    
    func onSendingPrediction() {
        
    }
    
    func onErrorConnection() {
        
    }
    
    func onErrorPrediction(errorMessage: String) {
       // AlertDialog.show(title: "Aviso", body: errorMessage, view: self)
    }
    
    
    func onSendingGeometry() {
        
    }
    
    func onSuccessGeometry(geoLatLon: Geometry, formatAddress: [AddressComponent]) {
        addrressComplete = formatAddress
        let camera = GMSCameraPosition.camera(withLatitude: (geoLatLon.location?.lat)!, longitude: (geoLatLon.location?.lng)!, zoom: 7.0)
        googleMapView.animate(to: camera)
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: (geoLatLon.location?.lat)!, longitude: (geoLatLon.location?.lng)!)
        marker.title = "Charls House"
        marker.snippet = "México"
        marker.map = googleMapView
        
        ViewUtils.setVisibility(view: self.resulTableView, visibility: .INVISIBLE)
    }
    
    func onErrorGeometry(errorMessage: String) {
        
    }

    func onErrorConnectionGeometry(){
    
    }
    
    
    func onItemClick(addressSelection: PredictionAddress) {
        self.geometryPresenter?.getLatLon(placeId: addressSelection.place_id!, googleMapKey: "AIzaSyA96e9yso7kBeWRujnxmOClOrKcatknH90")
        serchTextField.text = addressSelection.description_!
    }
    
    @IBAction func nextContinueButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller: ConfirmLocationViewController? = storyboard.instantiateViewController(withIdentifier: "ConfirmLocationViewController") as? ConfirmLocationViewController
        controller?.dataAddressComplete = addrressComplete
        navigationController?.pushViewController(controller!, animated: true)
    }
}
