//
//  ServiceAddOnDataSource.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 04/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import Foundation
import UIKit


class DescriptionPackageDataSource: NSObject, UITableViewDataSource, UITableViewDelegate{
    
    var tableView: UITableView?
    var descriptionArray : [DescriptionPackage] = []
    
    init(tableView:UITableView) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource=self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 60
        self.tableView?.rowHeight = UITableViewAutomaticDimension
    }
    
    func update(coleccionInfo: [DescriptionPackage]){
        self.descriptionArray = coleccionInfo
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return descriptionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "DescriptionPackageTableViewCell"
        
        let cell:DescriptionPackageTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier, for:indexPath)as! DescriptionPackageTableViewCell
        
        let description = self.descriptionArray[indexPath.row]
        let info = description.descriptionP
        let attrStr = try! NSAttributedString(
            data: (info?.data(using: String.Encoding.unicode, allowLossyConversion: true)!)!,
            options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
            documentAttributes: nil)
        
        cell.iconDescriptionImageView.image = UIImage(named:description.iconDescription!)
        cell.descriptionLabel.attributedText = attrStr
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60;//Choose your custom row height
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
        selectedCell.contentView.backgroundColor = UIColor.red
    }
    
    // if tableView is set in attribute inspector with selection to multiple Selection it should work.
    
    // Just set it back in deselect
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        var cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
        //cellToDeSelect.contentView.backgroundColor = UIColor(netHex: 0x5D758F)
    }
    
    
}

