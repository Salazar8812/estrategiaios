//
//  PredictionAddressDelegate.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 27/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit


public protocol PredictionAddressDelegate: NSObjectProtocol {
    
    func onSendingPrediction()
    
    func onSuccessPrediction(collectionAddress: [PredictionAddress])
    
    func onErrorPrediction(errorMessage : String)
    
    func onErrorConnection()
    
}

