//
//  AddOnService.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 04/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

public class AddOn: NSObject, Mappable {
    
    var idAddOn : String?
    var nameAddOn : String?
    var price : String?
   
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map : Map){
        
        idAddOn <- map["IdCard"]
        nameAddOn <- map["CardNumber"]
        price <- map["ExpirationMonth"]
    }
}
