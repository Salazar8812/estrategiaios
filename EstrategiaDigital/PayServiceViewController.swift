//
//  PayServiceViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 04/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class PayServiceViewController: UIViewController,iCarouselDataSource, iCarouselDelegate {


    @IBOutlet weak var listCardICarousel: iCarousel!
    @IBOutlet weak var addOnTableView: UITableView!
    
    var cardsArray = [Cards]()
    var addOnArray = [AddOn]()
    var selectedIndex : Int!
    var indice : Int!
    var serviceAddOnDS : ServiceAddOnDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populate_listCards()
        populate_AddOnList()
        
        listCardICarousel.type = iCarouselType.linear
        listCardICarousel.delegate = self
        listCardICarousel.dataSource = self
        listCardICarousel .reloadData()
        
        
        let nib = UINib(nibName: "ItemPagoTableViewCell", bundle: nil)
        addOnTableView.register(nib, forCellReuseIdentifier: "ItemPagoTableViewCell")
        serviceAddOnDS = ServiceAddOnDataSource(tableView: self.addOnTableView)
        serviceAddOnDS?.update(coleccionInfo: addOnArray)
        self.addOnTableView.reloadData()
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
    }

    func populate_listCards(){
        for i in 0 ... 5 {
            let c : Cards = Cards()
            c.cardNumber = "4152"
            c.expirationMonth = "12"
            c.expirationYear = "22"
            c.idCard = "1"
            c.name = "Carlos Salazar"
            cardsArray.append(c)
        }
    }
    
    func populate_AddOnList(){
        let c : AddOn = AddOn()
        c.idAddOn = "1"
        c.nameAddOn = "TV Adicional"
        c.price = "$ 89.00"
        addOnArray.append(c)
        
        let c2 : AddOn = AddOn()
        c2.idAddOn = "2"
        c2.nameAddOn = "HBO Max"
        c2.price = "$ 0.00"
        addOnArray.append(c2)
        
        let c3 : AddOn = AddOn()
        c3.idAddOn = "1"
        c3.nameAddOn = "Wifi Extender"
        c3.price = "$ 59.00"
        addOnArray.append(c3)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    /*-------------------------- Default Generate Methods iCarousel ------------------------*/
    
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return cardsArray.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let view = Bundle.main.loadNibNamed("ItemTypeCardCollectionReusableView", owner: self, options: nil)![0] as! ItemTypeCardCollectionReusableView
        /*view.frame.size = CGSize(width: self.view.frame.size.width/2, height: 83.0)
         view.backgroundColor = UIColor.lightGray*/
        
        return view
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        selectedIndex = index
        //self .performSegue(withIdentifier: "imageDisplaySegue", sender: nil)
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        indice = carousel.currentItemIndex
        
        do {
            if(carousel.currentItemIndex == 0){
                /*numbCard = cardsArray[0].cardNumber
                print("Numero de Tarjeta", numbCard!)*/
            }else{
                /*numbCard = cardsArray[carousel.currentItemIndex].cardNumber
                print("Numero de Tarjeta", numbCard!)*/
            }
            
        } catch let error as String{
            print(error)
        }
    }
    
    /*-------------------------- End Default Generate Methods iCarousel ------------------------*/

}
