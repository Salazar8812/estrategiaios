//
//  AddressComponent.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 28/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

public class AddressComponent : NSObject, Mappable{
    
    var long_name: String?
    var short_name : String?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        long_name		<- map["long_name"]
        short_name <- map["short_name"]
    }
}
