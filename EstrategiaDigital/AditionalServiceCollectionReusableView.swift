//
//  AditionalServiceCollectionReusableView.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 18/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class AditionalServiceCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var imageAdditionalImageView: UIImageView!

    @IBOutlet weak var legendLabel: UILabel!
    @IBOutlet weak var logoAdditionalImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
