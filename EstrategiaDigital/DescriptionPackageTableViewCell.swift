//
//  DescriptionPackageTableViewCell.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 05/07/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class DescriptionPackageTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var iconDescriptionImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
